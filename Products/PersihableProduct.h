#pragma once
#include "Product.h"
class PerishableProduct :
	public Product
{
public:
	PerishableProduct(const int32_t id,const std::string & name,const float rawPrice,const std::string & expirationDate);
	~PerishableProduct()=default;

	//Getters
	std::string GetExpirationDate()const;
	int32_t GetVAT()const override;
	float GetPrice()const override;

private:
	static const int32_t kVAT = 9;
	std::string m_expirationDate;
};

