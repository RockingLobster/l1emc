#include "NonPerishableProduct.h"

NonPerishableProduct::NonPerishableProduct(const int id, const std::string & name, const float rawPrice, NonPerishableProductType type)
	:Product(id,name,rawPrice), m_type(type)
{
}

NonPerishableProductType NonPerishableProduct::GetType() const
{
	return this->m_type;
}


int32_t NonPerishableProduct::GetVAT() const
{
	return kVAT;
}

float NonPerishableProduct::GetPrice() const
{
	return m_rawPrice + m_rawPrice * kVAT / 100.0;
}

std::string NonPerishableProduct::GetTypeName()const
{
	if (m_type == NonPerishableProductType::Clothing)
	{
		return  "Clothing";
	}
	if (m_type == NonPerishableProductType::SmallAppliances )
	{
		return "SmallAppliances";
	}
	if (m_type == NonPerishableProductType::PersonalHygiene )
	{
		return "PersonalHygiene";
	}
}

std::ostream & operator<<(std::ostream & out, const NonPerishableProduct & product)
{
	out << product.GetName() << " " << product.GetPrice() << " " << product.GetTypeName();
	return out;
}
