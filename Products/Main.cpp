#include <algorithm>
#include <vector>
#include <regex>
#include <memory>
#include "PersihableProduct.h"
#include "NonPerishableProduct.h"

bool isValidDateFormat(const std::string& date)
{
	return std::regex_match(date, std::regex("(\\d+)-(0|1)\\d-(0|1|2|3)\\d"));
}

NonPerishableProductType convertToNonPerishableProductType(const std::string & type)
{
	if (type == "Clothing")
	{
		return NonPerishableProductType::Clothing;
	}
	if (type == "SmallAppliances")
	{
		return NonPerishableProductType::SmallAppliances;
	}
	if (type == "PersonalHygiene")
	{
		return NonPerishableProductType::PersonalHygiene;
	}
}

int main()
{
	int32_t id;
	std::string name;
	float rawPrice;
	std::string perishableOrNonPerishable;

	std::ifstream input("product.prodb");
	std::vector<std::shared_ptr<IPriceable>> products;
	while (!input.eof())
	{
		input >> id >> name >> rawPrice >> perishableOrNonPerishable;
		if (isValidDateFormat(perishableOrNonPerishable))
		{
			products.push_back(std::make_shared<PerishableProduct>(id, name, rawPrice, perishableOrNonPerishable));
		}
		else
			products.push_back(std::make_shared<NonPerishableProduct>(id, name, rawPrice, convertToNonPerishableProductType(perishableOrNonPerishable)));
	}

	int choice = 1;
	std::cout << "Push 1 to sort by name\nPush 2 to sort by price\nPush 3 to print\nPush 0 to quit\n";
	while (choice != 0)
	{
		std::cin >> choice;
		switch (choice)
		{
		case 0:
			break;
		case 1:
			std::sort(products.begin(), products.end(), [](const std::shared_ptr <IPriceable> firstProduct,const std::shared_ptr<IPriceable> secondProduct) {
				return std::dynamic_pointer_cast<Product>(firstProduct)->GetName()<std::dynamic_pointer_cast<Product>(secondProduct)->GetName();
			});
			std::cout << "sorted by name\n";
			break;
		case 2:
			std::sort(products.begin(), products.end(), [](const std::shared_ptr <IPriceable> firstProduct, const std::shared_ptr<IPriceable> secondProduct) {
				return std::dynamic_pointer_cast<Product>(firstProduct)->GetPrice() < std::dynamic_pointer_cast<Product>(secondProduct)->GetPrice();
			});
			std::cout << "sorted by price\n";
			break;
		case 3:
			for (auto iterator : products)
			{
				auto nonPerishableProduct = std::dynamic_pointer_cast<NonPerishableProduct>(iterator);
				if (nonPerishableProduct != nullptr)
					std::cout << *nonPerishableProduct << "\n";
			}
			break;
		}
		std::cout << "\n";
	}
	
	return 0;
}