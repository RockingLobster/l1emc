#pragma once
#include <cstdint>
#include <string>
#include <iostream>
#include <fstream>
class IPriceable
{
public:
	virtual ~IPriceable() = default;
	virtual int32_t GetVAT()const=0;
	virtual float GetPrice()const=0;
};

