#pragma once
#include "IPriceable.h"
class Product :
	public IPriceable
{

public:
	Product(int32_t  c_id,const std::string & c_name,float  c_rawPrice);

	//Getters
	int32_t GetID()const;
	std::string GetName()const;
	float GetRawPrice()const;

protected:
	int32_t m_id;
	std::string m_name;
	float m_rawPrice;
};

