#include "Product.h"




Product::Product(int32_t  c_id, const std::string & c_name,float c_rawPrice)
	:m_id(c_id),m_name(c_name),m_rawPrice(c_rawPrice)
{
}

int32_t Product::GetID() const
{
	return m_id;
}

std::string Product::GetName() const
{
	return m_name;
}

float Product::GetRawPrice() const
{
	return m_rawPrice;
}
