#pragma once
#include "Product.h"
#include "NonperishableProductType.h"
class NonPerishableProduct :
	public Product
{
public:
	NonPerishableProduct(const int id,const std::string & name,const float rawPrice,NonPerishableProductType type);
	~NonPerishableProduct()=default;

	//Getters
	NonPerishableProductType GetType()const;
	std::string GetTypeName()const;
	int32_t GetVAT()const override;
	float GetPrice()const override;

	friend std::ostream & operator <<(std::ostream & out, const NonPerishableProduct & product);

private:
	static const int32_t kVAT = 9;
	NonPerishableProductType m_type;
};

