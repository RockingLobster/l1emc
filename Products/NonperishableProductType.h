#pragma once

enum class NonPerishableProductType
{
	Clothing = 0,
	SmallAppliances,
	PersonalHygiene
};