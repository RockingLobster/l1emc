#include "PersihableProduct.h"

PerishableProduct::PerishableProduct(const int32_t id, const std::string & name,const float rawPrice, const std::string & expirationDate)
	:Product::Product(id, name, rawPrice), m_expirationDate(expirationDate)
{
}

std::string PerishableProduct::GetExpirationDate() const
{
	return m_expirationDate;
}

int32_t PerishableProduct::GetVAT() const
{
	return kVAT;
}

float PerishableProduct::GetPrice() const
{
	return m_rawPrice + m_rawPrice * kVAT / 100.0;
}

